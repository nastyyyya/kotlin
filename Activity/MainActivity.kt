import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button_submit.setOnClickListener {
            val firstName = edittext_first_name.text.toString()
            val lastName = edittext_last_name.text.toString()
            val middleName = edittext_middle_name.text.toString()
            val age = edittext_age.text.toString()
            val hobby = edittext_hobby.text.toString()

            val intent = Intent(this, InfoActivity::class.java).apply {
                putExtra("first_name", firstName)
                putExtra("last_name", lastName)
                putExtra("middle_name", middleName)
                putExtra("age", age)
                putExtra("hobby", hobby)
            }
            startActivity(intent)
        }
    }
}
