//Сделать программу, которая запускает в фоновом потоке метод печати «World» с задержкой в 1 сек.
// В основном потоке запускаем печать «Hello,» с задержкой в 2 сек., блокируем поток
import kotlinx.coroutines.*
fun main(args: Array<String>) = runBlocking<Unit> {

    launch (Dispatchers.Default) {
        delay(1000)
        println("World!")
    }
    
    delay(2000)
    println("Hello, ")

}

//Написать две функции с задержкой, которые будут возвращать 2 числа. 
//В main написать блок кода, который будет суммировать вызов этих 2 функций, 
//сначала написать последовательный вызов функций, вывести сумму и время работы, 
//потом написать блок с асинхронным вызовом и сравнить время работы, обосновать время
import kotlinx.coroutines.*

suspend fun firstNum(): Int {
    delay(1000)
    return 100
}

suspend fun secondNum(): Int {
    delay(1500)
    return 50
}

fun main() = runBlocking {

    val startTime = System.currentTimeMillis()

    val firstNum = firstNum()
    val secondNum = secondNum()

    val endTime = System.currentTimeMillis()

    println(firstNum + secondNum)
    println(endTime - startTime)

//async

    val asyncStartTime = System.currentTimeMillis()

    val asyncFirstNum = async { firstNum() }
    val asyncSecondNum = async { secondNum() }

    println(asyncFirstNum.await() + asyncSecondNum.await())
    val asyncEndTime = System.currentTimeMillis()
    println(asyncEndTime - asyncStartTime)
}


//Дополнить код, чтоб программа выводила след текст в консоль.
//«I'm sleeping 0 ...I'm sleeping 1 ... I'm sleeping 2 ... main: I'm tired of waiting! I'm running finally main: Now I can quit.»

const val decorator = "///////////////////////////"

fun main(args: Array<String>) = runBlocking<Unit> {
    println(decorator)

    val job = launch {
        try {
            repeat(times = 3) { i ->
                println("I'm sleeping $i ...")
                delay(1000)
            }

        } finally {
            println("I'm running finally")
            delay(1500)
        }
    }

    delay(2500)
    println("main: I'm tired of waiting!")

    job.join()

    println("main: Now I can quit")

    println(decorator)
}

