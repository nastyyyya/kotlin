import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    private lateinit var imageView: ImageView
    private lateinit var btnChangeImage: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageView = findViewById(R.id.imageView)
        btnChangeImage = findViewById(R.id.btnChangeImage)

        btnChangeImage.setOnClickListener {
            val randomImage = getRandomImage()
            imageView.setImageResource(randomImage)

            // change button color
            val randomColor = getRandomColor()
            btnChangeImage.setBackgroundColor(randomColor)
        }
    }

    private fun getRandomImage(): Int {
        val images = arrayOf(R.drawable.cat1, R.drawable.cat2, R.drawable.cat3)
        return images.random()
    }

    private fun getRandomColor(): Int {
        val r = (0..255).random()
        val g = (0..255).random()
        val b = (0..255).random()
        return Color.rgb(r, g, b)
    }
}
